// example.test.js
const expect = require('chai').expect
const assert = require('chai').assert
const { should } = require('chai')
const mylib = require('../src/mylib')

describe('Testataan mylib.js', () => {
    let myvar = undefined

    before(() => {
        myvar = "!"
    })

    after(() => console.log('Mylib testit ohi'));

    it('Assert mylib.write is "Tervetuloa !" with myvar', () => {
        assert.equal(mylib.write(myvar), 'Tervetuloa !');
    })

    it('Should return 0 when 10%5', () =>  {
        expect(mylib.jakojaan(10,5)).to.equal(0);
    })








})